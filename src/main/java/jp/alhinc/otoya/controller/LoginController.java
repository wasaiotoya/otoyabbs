package jp.alhinc.otoya.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.alhinc.otoya.form.LoginForm;
import jp.alhinc.otoya.validation.GroupOrder;

@Controller
public class LoginController {
	@Autowired
    private JdbcTemplate jdbcTemplate;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model, @Validated(GroupOrder.class) @ModelAttribute("loginForm") LoginForm loginForm,
			BindingResult result) {
		if (result.hasErrors()) {
			return "login";
		}
		List<Map<String, Object>> list = jdbcTemplate.queryForList("SELECT * FROM users WHERE name = ?  AND password = ?",loginForm.getLoginName(), loginForm.getLoginPassword());
		
		if (list.size() > 0) {
			model.addAttribute("userName", list.get(0).get("name"));
			return "home";
		} else {
			return "login";
		}
	}
}