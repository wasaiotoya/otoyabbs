package jp.alhinc.otoya.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jp.alhinc.otoya.model.User;

@Repository
public interface UsersRepository  extends JpaRepository<User,String>{
	public List<User> findById(int id);
}
